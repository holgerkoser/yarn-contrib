import type { CommandContext } from '@yarnpkg/core';
import type { Usage } from 'clipanion';
import { Command } from 'clipanion';
export declare class ProdInstall extends Command<CommandContext> {
    static paths: string[][];
    outDirectory: string;
    json: boolean;
    stripTypes: boolean;
    pack: boolean;
    silent: boolean;
    production: boolean;
    static usage: Usage;
    execute(): Promise<0 | 1>;
    private getPatchSourcesToRemove;
    private modifyOriginalResolutions;
}
